package b137.mendez.s04d1;

public class Main {
    public static void main(String[] args) {
        Car firstCar = new Car();
        firstCar.setName("Forte Ex Sedan");
        firstCar.setBrand("KIA");
        firstCar.setYearOfMake(2020);

        firstCar.drive();
        /*System.out.println(firstCar.getName());
        System.out.println(firstCar.getBrand());
        System.out.println(firstCar.getYearOfMake());*/

        Car secondCar = new Car("Avanza", "Toyota", 2005);
        secondCar.drive();


    }
}
